# Rock Paper Scissors Game

## Instructions
1. Install node(5.8.0) and npm(3.7.3)
You can use [nvm](https://github.com/creationix/nvm) (node version manager) to install different versions of node
2. Install gulp globall running: npm install gulp -g
3. Run: npm install
4. Run: gulp dev and navigate to the [game](http://localhost:8000/#/). Use the logo to go back!
5. To run the tests run: gulp test

## Architecture
### Code structure
#### Components
The code follows a componentised structure. Each component is each own mvvm app and it adheres to its own routes. It has its own tests, styles and views.
#### Application
The application is a single page app that requires all the components needed and their styles. Essentially is the entry point.
### Build pipeline
I utilized a build pipeline that I have written previously. It all relies on gulp tasks. To bundle the code it uses browserify and babel as the code is written in es6.
### Design Patterns
#### MVC/MVVM
The applications is written in a three tier architecture to split the business logic from the view and the data tier. It is not a clear MVC as the the view knows about the controller through the view-model that is passed to it. The framework is pretty basic but it helps to decouple the code.
##### Router
The router is pretty basic. It is using the hash symbol to achieve routing between controllers. It subscribes to the 'onhashchange' native event, and every time there is a change it triggers a dispatch trying to match the path with any registered routes.
##### View
All the views should extend the base View class and implement a few methods. The template method which should return the string html template. Lastly the onLoad method which is used to attach eventHandlers to any DOM elements after they have been loaded in the view.
##### Controller
All Controllers are simple classes and act as the view-model (glue) between the View and the business logic.
#### Strategy design pattern
There are two modes in the game, a player vs computer and computer vs computer. Each of the players has to extend the base Player class and implement a getSelection method. Each player has each own strategy of how to return the selection all it has to do is to overload the base Player getSelection method.
#### RPC engine
The engine is fairly trivial. It is using a map of combinations to store the different combinations. So each time a result is needed there is no need for control flow style code (if else) just access the combinations map with the selection keys. It works like a state-container holding all the combinations in a tree of choices. Traversing it is cheap. Also adding more combinations is not hard so it can scale fairly easily.

## Methodology
### MVP
Initially formed a tiny MVP, broke down the epic into small user stoties and subtasks. Created the story board on paper and then fleshed out the wireframes in html. I tried to make a bit responsive without using media queries just having a simple UI that scales relative to the size of the screen. Tried to estimate them and prioritize them and ruled out any overkills (Continues delivery, leaderboard) that wouldnt be able to be finished within a week.
### CI
Used gitlab as it provides ci-runners and with a minimal setup I could achieve continues integration. All the merge requests were merged only if they were passing the build. Passing the build === Pass unit tests, meet code style and linting standards via jshint and jscs.
### Branching strategy
Used a light gitflow. Just develop and feature branches.
### Agile
Created a trello agile board and populated it with my stories. Timeboxed all boilerplate tasks to 3 hours so I dont lose motivation just setting up the project.
### TDD
Followed TDD only when developed the mvvm component as it was interesting and also fairly standard, so minimal changes where required, plus doing the technical design before hand helped a lot to flesh out my test cases before i write my code.
The rpc component kept changing all the time in terms of the business logic and the structure and found tdd quite impeding as changing the tests was required every time I had to experiment. I guess tdd is not for POC's :-)
## Future improvements
1. Implement a pubsub to achieve an event driven architecture rather than a state based
2. Setup a server and have a continues deliver pipeline on every push event on the develop branch.
3. Improve the router, to support params and nested routes.
4. Decouple the GamePage view to more subviews as now it holds a lot of logic.
5. Improve the UI, make it responsive, and mobile first. Improve the animations.
6. Implement a persistence layer, to be able to support additional features like leaderboard, scoreboard, history
7. Stretch goal make a smarter bot with predictive ability based on bayesian network.
8. Fix the orientation of the icons so they are mirrored in the correct direction, as opposing players.
9. Increase the test coverage in views.
10. Move the rpc-engine into its own component
