'use strict';
import View from '../mvvm/View';
export default class LandingPageView extends View {
    constructor($vm) {
        super($vm);
    }
    template() {
        return `
            <div class="container" id="landing-page">
                <div id="logo">
                    <i class="fa fa-hand-rock-o" aria-hidden="true"></i>
                    <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                    <i class="fa fa-hand-scissors-o" aria-hidden="true"></i>
                </div>
                <div class="inner-container">
                    <a href="${this.$vm.playerVsComputerLink}" class="game-mode left">
                        <i class="user icon fa fa-user"></i>
                        <div class="versus"><span>VS</span></div>
                        <i class="computer icon fa fa-desktop"></i>
                    </a>
                    <a href="${this.$vm.computerVsComputerLink}" class="game-mode right">
                        <i class="computer icon fa fa-desktop"></i>
                        <div class="versus"><span>VS</span></div>
                        <i class="computer icon fa fa-desktop"></i>
                    </a>
                    <div class="clear-fix"></div>
                </div>
            </div>
        `;
    }
}