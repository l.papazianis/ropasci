export default class LandingPageController {
    constructor() {
        this.playerVsComputerLink = '#/player-vs-computer';
        this.computerVsComputerLink = '#/computer-vs-computer';
    }
}