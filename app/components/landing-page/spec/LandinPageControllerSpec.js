'use strict';

import LandingPageController from '../LandingPageController.js';

describe('LandingPageController', () => {
    it('Should exist', () => {
        expect(LandingPageController).toBeDefined();
        expect(typeof LandingPageController).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should have the playerVsComputerLink in its context', () => {
            let controller = new LandingPageController();
            expect(controller.playerVsComputerLink).toBeDefined();
            expect(controller.playerVsComputerLink).toBe('#/player-vs-computer');
        });
        it('Should have the computerVsComputerLink in its context', () => {
            let controller = new LandingPageController();
            expect(controller.computerVsComputerLink).toBeDefined();
            expect(controller.computerVsComputerLink).toBe('#/computer-vs-computer');
        });
    });
});