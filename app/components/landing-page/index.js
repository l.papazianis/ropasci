'use strict';

import App from '../mvvm/';
import LandingPageController from './LandingPageController';
import LandingPageView from './LandingPageView';

const app = new App('#wrapper');
app.router.addRoute('/', LandingPageController, LandingPageView);
app.bootstrap();