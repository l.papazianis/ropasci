'use strict';

import Router from './Router';
export default class App {
    constructor(container) {
        this.container = container;
        this.router = new Router(container);
    }
    bootstrap() {
        this.router._start();
        this.router._reload();
    }
}