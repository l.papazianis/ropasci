'use strict';
export default class View {
    constructor($vm) {
        this.$vm = $vm;
    }
    render(container) {
        document.querySelector(container).innerHTML = this.template();
        if (this.onLoad) {
            this.onLoad();
        }
    }
}