'use strict';
import Route from './Route';
export default class Router {
    constructor(container) {
        this.routes = {};
        this.container = container;
    }
    _getHash() {
        return window.location.hash.substring(1);
    }
    _dispatch(route = this.default) {
        if (route) {
            route.go().render(this.container);
        }
    }
    _hashChanged() {
        let result = Object.keys(this.routes)
        .filter(path => this.routes[path].match(this._getHash()));
        this._dispatch(this.routes[result[0]]);
    }
    _reload() {
        this._hashChanged();
    }
    addRoute(path, Controller, View) {
        if (!this.routes[path]) {
            this.routes[path] = new Route(path, Controller, View);
        } else {
            throw new Error('This path has already a handler and a view');
        }
    }
    _start() {
        if (window.addEventListener) {
            window.addEventListener('hashchange', () => this._hashChanged(), false);
        } else {
            window.attachEvent('onhashchange', () => this._hashChanged());
        }
    }
    otherwise(path, Controller, View) {
        this.default = new Route(path, Controller, View);
    }
}