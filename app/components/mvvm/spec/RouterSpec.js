'use strict';
import Router from '../Router';
import Route from '../Route';
describe('Router', () => {
    it('Should exist', () => {
        expect(Router).toBeDefined();
        expect(typeof Router).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should have an empty object named routes as a property', () => {
            let router = new Router();
            expect(router.routes).toBeDefined();
            expect(typeof router.routes).toBe('object');
            expect(Object.keys(router.routes).length).toBe(0);
        });
        describe('getHash method', () => {
            let router = new Router();
            it('Should exist', () => {
                expect(router._getHash).toBeDefined();
                expect(typeof router._getHash).toBe('function');
            });
            describe('When called', () => {
                it('Should return the hash property of the window location object without the hash', () => {
                    window.location.hash = '#/testing';
                    expect(router._getHash()).toBe('/testing');
                });
            });
        });
        describe('_dispatch method', () => {
            let router = new Router();
            it('Should exist', () => {
                expect(router._dispatch).toBeDefined();
                expect(typeof router._dispatch).toBe('function');
            });
            describe('When invoked with a route object', () => {
                it('Should call the routes go method', () => {
                    var mockView = {
                        render: () => {},
                        template: () => {}
                    };
                    var mockRoute = {
                        go: () => {}
                    };
                    spyOn(mockRoute, 'go').and.returnValue(mockView);
                    router._dispatch(mockRoute);
                    expect(mockRoute.go).toHaveBeenCalled();
                });
                it('Should call the returned views render method', () => {
                    var mockView = {
                        render: () => {},
                        template: () => {}
                    };
                    var mockRoute = {
                        go: () => {}
                    };
                    spyOn(mockRoute, 'go').and.returnValue(mockView);
                    spyOn(mockView, 'render').and.callThrough();
                    router._dispatch(mockRoute);
                    expect(mockView.render).toHaveBeenCalled();
                });
            });
        });
        describe('_hashChanged method', () => {
            it('Should exist', () => {
                var router = new Router();
                expect(router._hashChanged).toBeDefined();
                expect(typeof router._hashChanged).toBe('function');
            });
            describe('When invoked', () => {
                it('Should dispatch the route that its path matches the hash of the window', () => {
                    var router = new Router();
                    window.location.hash = '#/test';
                    router.addRoute('/test', null, null);
                    spyOn(router, '_dispatch').and.callFake(() => {});
                    router._hashChanged();
                    expect(router._dispatch).toHaveBeenCalledWith(router.routes['/test']);
                });
            });
        });
        describe('_reload method', () => {
            it('Should exist', () => {
                var router = new Router();
                expect(router._reload).toBeDefined();
                expect(typeof router._reload).toBe('function');
            });
            describe('When invoked', () => {
                it('Should invoke hashChanged', () => {
                    var router = new Router();
                    spyOn(router, '_hashChanged').and.callFake(() => {});
                    router._reload();
                    expect(router._hashChanged).toHaveBeenCalled();
                });
            });
        });
        describe('addRoute method', () => {
            it('Should exist', () => {
                var router = new Router();
                expect(router.addRoute).toBeDefined();
                expect(typeof router.addRoute).toBe('function');
            });
            describe('When invoked', () => {
                describe('When there is no previously defined route under the specified path key', () => {
                    it('Should create a new route instance and store in the routes map under the' +
                    ' specified path key', () => {
                        var router = new Router();
                        var mockController = null;
                        var mockView = null;
                        router.addRoute('/test', mockController, mockView);
                        expect(router.routes['/test'] instanceof Route).toBeTruthy();
                        expect(router.routes['/test'].View).toBe(mockView);
                        expect(router.routes['/test'].Controller).toBe(mockController);
                    });
                });
                describe('When there is a previously defined route under the specified path key', () => {
                    it('Should throw an exception', () => {
                        var router = new Router();
                        var mockController = null;
                        var mockView = null;
                        router.addRoute('/test', mockController, mockView);
                        const mockError = () => router.addRoute('/test', mockController, mockView);
                        expect(mockError)
                        .toThrowError(Error, 'This path has already a handler and a view');
                    });
                });
            });
        });
        describe('_start method', () => {
            it('Should exist', () => {
                var router = new Router();
                expect(router._start).toBeDefined();
                expect(typeof router._start).toBe('function');
            });
        });
        describe('otherwise method', () => {
            it('Should exist', () => {
                var router = new Router();
                expect(router.otherwise).toBeDefined();
                expect(typeof router.otherwise).toBe('function');
            });
            describe('When invoked', () => {
                it('Should setup a default route under the default property of the instance of the router', () => {
                    var router = new Router();
                    router.otherwise('/path', null, null);
                    expect(router.default).toBeDefined();
                    expect(router.default instanceof Route).toBeTruthy();
                    expect(router.default.path).toBe('/path');
                    expect(router.default.View).toBe(null);
                    expect(router.default.Controller).toBe(null);
                });
            });
        });
    });
});