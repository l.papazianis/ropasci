'use strict';

import View from '../View.js';

describe('View', () => {
    it('Should exist', () => {
        expect(View).toBeDefined();
        expect(typeof View).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should store the passed view model from the constructor to its context', () => {
            var viewmodel = {};
            var view = new View(viewmodel);
            expect(view.$vm).toBe(viewmodel);
        });
        describe('render method', () => {
            it('Should exist', () => {
                var viewmodel = {};
                var view = new View(viewmodel);
                expect(view.render).toBeDefined();
                expect(typeof view.render).toBe('function');
            });
            describe('When called', () => {
                it('Should call the onLoad and the template method of the view and ' +
                'set the innerHTML of the element with what the template returns', () => {
                    var viewmodel = {};
                    var element = {};
                    var mockTemplate = 'bla';
                    window.document.querySelector = () => element;
                    View.container = '#container';
                    class TestView extends View {
                        constructor($vm) {
                            super($vm);
                        }
                        template() {
                            return mockTemplate;
                        }
                        onLoad() {

                        }
                    }
                    var view = new TestView(viewmodel);
                    spyOn(view, 'onLoad').and.callThrough();
                    spyOn(view, 'template').and.callThrough();
                    view.render();
                    expect(view.onLoad).toHaveBeenCalled();
                    expect(view.template).toHaveBeenCalled();
                    expect(element.innerHTML).toBe(mockTemplate);
                });
            });
        });
    });
});