'use strict';

import Route from '../Route';
class TestController {
    constructor() {

    }
}
class TestView {
    constructor() {

    }
}
describe('Route', () => {
    it('Should exist', () => {
        expect(Route).toBeDefined();
        expect(typeof Route).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should store the path and the handler to its context', () => {
            var expectedPath = '/test';
            var route = new Route('/test', TestController, TestView);
            expect(route.path).toBe(expectedPath);
            expect(route.Controller).toBe(TestController);
            expect(route.View).toBe(TestView);
        });
        describe('match method', () => {
            it('Should exist', () => {
                var expectedPath = '/test';
                var route = new Route(expectedPath, TestController, TestView);
                expect(route.match).toBeDefined();
                expect(typeof route.match).toBe('function');
            });
            describe('When invoked', () => {
                it('Should return true if the given hash matches the path',() => {
                    var expectedPath = '/test';
                    var route = new Route(expectedPath, TestController, TestView);
                    expect(route.match(expectedPath)).toBeTruthy();
                });
                it('Should return false if the given hash matches the path',() => {
                    var expectedPath = '/test';
                    var route = new Route(expectedPath, TestController, TestView);
                    expect(route.match('/test2')).toBeFalsy();
                });
            });
        });
        describe('go method', () => {
            it('Should exist', () => {
                var expectedPath = '/test';
                var route = new Route(expectedPath, TestController, TestView);
                expect(route.go).toBeDefined();
                expect(typeof route.go).toBe('function');
            });
            describe('When invoked', () => {
                it('Should return a new instance of the view passing in the ' +
                'constructor of the view the controller as a view model', () => {
                    var expectedPath = '/test';
                    var route = new Route(expectedPath, TestController, TestView);
                    var result = route.go();
                    expect(result instanceof TestView).toBeTruthy();
                });
            });
        });
    });
});