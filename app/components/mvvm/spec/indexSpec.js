'use strict';

import App from '../';
import Router from '../Router';
describe('Index', () => {
    it('Should exist', () => {
        expect(App).toBeDefined();
        expect(typeof App).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should store the container to its context', () => {
            const container = '#example';
            const app = new App(container);
            expect(app.container).toBeDefined();
            expect(app.container).toBe(container);
        });
        it('Should have a new instance of the router in its context', () => {
            const container = '#example';
            const app = new App(container);
            expect(app.router).toBeDefined();
            expect(app.router instanceof Router).toBeTruthy();
        });
        describe('bootstrap method', () => {
            it('Should exist', () => {
                const container = '#example';
                const app = new App(container);
                expect(app.bootstrap).toBeDefined();
                expect(typeof app.bootstrap).toBe('function');
            });
            describe('When invoked', () => {
                it('Should call the _start method of the router', () => {
                    const container = '#example';
                    const app = new App(container);
                    spyOn(app.router, '_start').and.callFake(() => {});
                    spyOn(app.router, '_reload').and.callFake(() => {});
                    app.bootstrap();
                    expect(app.router._start).toHaveBeenCalled();
                });
                it('Should call the _reload method of the router', () => {
                    const container = '#example';
                    const app = new App(container);
                    spyOn(app.router, '_start').and.callFake(() => {});
                    spyOn(app.router, '_reload').and.callFake(() => {});
                    app.bootstrap();
                    expect(app.router._reload).toHaveBeenCalled();
                });
            });
        });
    });
});