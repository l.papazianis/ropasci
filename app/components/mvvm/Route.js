export default class Route {
    constructor(path, Controller, View) {
        this.path = path;
        this.Controller = Controller;
        this.View = View;
    }
    match(hash) {
        return (this.path === hash);
    }
    go() {
        return new this.View(new this.Controller());
    }
}