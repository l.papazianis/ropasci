'use strict';

import RPC from './rpc-engine';
import Human from './Human';
import Bot from './Bot';

export default class PvsCController {
    constructor() {
        this.mode = 'PvsC';
        this.rpc = new RPC(this.mode);
        this.player1 = new Human();
        this.player2 = new Bot();
    }
    select(event) {
        let selections = document.getElementsByClassName('selection');
        for (let selection of selections) {
            selection.classList.remove('selected');
        }
        event.target.classList.add('selected');
        this.player1.setSelection(Number(event.target.dataset.selection));
    }
    play() {
        if (!this.player1.selection) {
            alert('Please select rock, paper or scissors!');
            return false;
        } else {
            return this.rpc.play(this.player1, this.player2);
        }
    }
    reset() {
        document.querySelector('.selection-tab').classList.remove('hide');
        let selections = document.getElementsByClassName('selection');
        for (let selection of selections) {
            selection.classList.remove('selected');
        }
        delete this.player1.selection;
    }
    loading() {
        document.querySelector('.selection-tab').classList.add('hide');
    }
}