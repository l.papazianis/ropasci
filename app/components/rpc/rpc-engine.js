'use strict';

import {COMBINATIONS} from './combinations';

export default class RPC {
    constructor(mode) {
        this.mode = mode;
    }
    play(player1, player2) {
        return this._getResults(player1.getSelection(), player2.getSelection());
    }
    _getResults(s1, s2) {
        return {
            player1: {
                selection: s1,
                result: COMBINATIONS[s1][s2]
            },
            player2: {
                selection: s2,
                result: COMBINATIONS[s2][s1]
            }
        };
    }
}