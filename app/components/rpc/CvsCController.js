'use strict';

import RPC from './rpc-engine';
import Bot from './Bot';

export default class CvsCController {
    constructor() {
        this.mode = 'CvsC';
        this.rpc = new RPC(this.mode);
        this.player1 = new Bot();
        this.player2 = new Bot();
    }
    play() {
        return this.rpc.play(this.player1, this.player2);
    }
}