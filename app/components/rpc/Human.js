'use strict';

import Player from './Player';

export default class Human extends Player {
    constructor() {
        super();
    }
    getSelection() {
        return super.getSelection(this.selection);
    }
    setSelection(number) {
        this.selection = number;
    }
}