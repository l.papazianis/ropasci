'use strict';

export const CHOICES = {
    1: 'ROCK',
    2: 'PAPER',
    3: 'SCISSORS'
};