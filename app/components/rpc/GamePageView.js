'use strict';

import View from '../mvvm/View';

export default class GamePageView extends View {
    constructor($vm) {
        super($vm);
        this.templates = {
            'ROCK': '<i class="fa fa-hand-rock-o"></i>',
            'PAPER': '<i class="fa fa-hand-paper-o"></i>',
            'SCISSORS': '<i class="fa fa-hand-scissors-o"></i>'
        };
    }
    template() {
        return `
            <div class="container" id="game-page">
                <div id="logo">
                    <a href="#/">
                        <i class="fa fa-hand-rock-o" aria-hidden="true"></i>
                        <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                        <i class="fa fa-hand-scissors-o" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="inner-container">
                    <div class="player left">
                        <div class="slot">
                            <div class="inner">
                                <i class="fa fa-hand-rock-o"></i>
                            </div>
                        </div>
                        <div class="hide selection-tab">
                            <div class="button selection fa fa-hand-rock-o" id="rock" data-selection="1">
                            </div>
                            <div class="button selection fa fa-hand-paper-o" id="paper" data-selection="2">
                            </div>
                            <div class="button selection fa fa-hand-scissors-o" id="scissors" data-selection="3">
                            </div>
                            <div class="clear-fix"></div>
                        </div>
                    </div>
                    <div class="player right">
                        <div class="slot">
                            <div class="inner">
                                <i class="fa fa-hand-rock-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div id="play">
                        <i class="fa fa-youtube-play"></i>
                    </div>
                    <div id="re-play" class="hide">
                        <i class="fa fa-repeat"></i>
                    </div>
                    <div class="result player1 hide">
                        <h1></h1>
                    </div>
                    <div class="result player2 hide">
                        <h1></h1>
                    </div>
                </div>
            </div>
        `;
    }
    loading() {
        if (this.$vm.loading) {
            this.$vm.loading();
        }
        document.querySelector('#play').classList.add('hide');
        let selections = document.getElementsByClassName('inner');
        for (let selection of selections) {
            selection.classList.add('shake');
        }
        return new Promise((resolve) => {
            setTimeout(()=> {
                document.querySelector('#re-play').classList.remove('hide');
                let selections = document.getElementsByClassName('inner');
                for (let selection of selections) {
                    selection.classList.remove('shake');
                }
                resolve();
            }, 2000);
        });
    }
    showResult(player1, player2) {
        document.querySelector('.result.player1').innerHTML = `<h1>${player1.result}!</h1>`;
        document.querySelector('.result.player2').innerHTML = `<h1>${player2.result}!</h1>`;
        document.querySelector('.player.left .slot .inner').innerHTML = this.templates[player1.selection];
        document.querySelector('.player.right .slot .inner').innerHTML = this.templates[player2.selection];
        let selections = document.getElementsByClassName('result');
        for (let selection of selections) {
            selection.classList.remove('hide');
        }
    }
    onLoad() {
        if (this.$vm.mode === 'PvsC') {
            document.querySelector('.selection-tab').classList.remove('hide');
        }

        const selectionClickHandler = event => this.$vm.select(event);
        const playClickHandler = () => {
            let result = this.$vm.play();
            if (result) {
                this.loading()
                .then(() => this.showResult(result.player1, result.player2));
            }
        };
        const resetEventHandler = () => {
            document.querySelector('#re-play').classList.add('hide');
            document.querySelector('#play').classList.remove('hide');
            document.querySelector('.result.player1').classList.add('hide');
            document.querySelector('.result.player2').classList.add('hide');
            document.querySelector('.player.right .slot .inner').innerHTML = this.templates.ROCK;
            document.querySelector('.player.left .slot .inner').innerHTML = this.templates.ROCK;
            if (this.$vm.reset) {
                this.$vm.reset();
            }
        };
        const clickEventType = 'click';
        document.getElementById('rock').addEventListener(clickEventType, selectionClickHandler, false);
        document.getElementById('paper').addEventListener(clickEventType, selectionClickHandler, false);
        document.getElementById('scissors').addEventListener(clickEventType, selectionClickHandler, false);
        document.getElementById('play').addEventListener(clickEventType, playClickHandler, false);
        document.getElementById('re-play').addEventListener(clickEventType, resetEventHandler, false);
    }
}