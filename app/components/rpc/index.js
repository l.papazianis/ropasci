'use strict';

import App from '../mvvm/';
import CvsCController from './CvsCController';
import PvsCController from './PvsCController';
import GamePageView from './GamePageView';

const app = new App('#wrapper');
app.router.addRoute('/player-vs-computer', PvsCController, GamePageView);
app.router.addRoute('/computer-vs-computer', CvsCController, GamePageView);
app.bootstrap();