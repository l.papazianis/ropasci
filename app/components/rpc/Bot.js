'use strict';

import Player from './Player';

export default class Bot extends Player {
    constructor() {
        super();
    }
    getSelection() {
        return super.getSelection(Math.floor(Math.random() * (3 - 1 + 1) + 1));
    }
}