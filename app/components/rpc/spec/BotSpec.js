'use strict';

import Player from '../Player';
import Bot from '../Bot';
describe('Bot', () => {
    it('Should exist', () => {
        expect(Bot).toBeDefined();
        expect(typeof Bot).toBe('function');
    });
    describe('When instanciated', () => {
        it('Should be an instance of Player', () => {
            let pc = new Bot();
            expect(pc instanceof Player).toBeTruthy();
        });
        it('Should have a getSelection method', () => {
            let pc = new Bot();
            expect(pc.getSelection).toBeDefined();
            expect(typeof pc.getSelection).toBe('function');
        });
        describe('When getSelection method is called', () => {
            it('Should call the parent getSelection method with a number between 1 and 3', () => {
                let pc = new Bot();
                spyOn(Player.prototype, 'getSelection').and.callFake(() => {});
                pc.getSelection();
                let args = Player.prototype.getSelection.calls.allArgs()[0][0];
                expect((args <=3) && (args >= 1)).toBeTruthy();
                expect(Player.prototype.getSelection).toHaveBeenCalled();
            });
        });
    });
});