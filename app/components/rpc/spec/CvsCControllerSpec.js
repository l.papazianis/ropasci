'use strict';

import CvsCController from '../CvsCController';
import RPC from '../rpc-engine';
import Bot from '../Bot';

describe('CvsCController', () => {
    it('Should exist', () => {
        expect(CvsCController).toBeDefined();
        expect(typeof CvsCController).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should have the correct mode set', () => {
            var c = new CvsCController();
            expect(c.mode).toBe('CvsC');
        });
        it('Should have an instance of the RPC engine set', () => {
            var c = new CvsCController();
            expect(c.rpc instanceof RPC).toBeTruthy();
        });
        it('Should have a bot as a player 1', () => {
            var c = new CvsCController();
            expect(c.player1 instanceof Bot).toBeTruthy();
        });
        it('Should have a bot as a player 2', () => {
            var c = new CvsCController();
            expect(c.player2 instanceof Bot).toBeTruthy();
        });
        it('Should have a play method', () => {
            var c = new CvsCController();
            expect(c.play).toBeDefined();
            expect(typeof c.play).toBe('function');
        });
        describe('When the play method is called', () => {
            it('Should call the rpc play method with the two players', () => {
                var c = new CvsCController();
                spyOn(c.rpc, 'play').and.callFake(() => {});
                c.play();
                expect(c.rpc.play).toHaveBeenCalledWith(c.player1, c.player2);
            });
        });
    });
});