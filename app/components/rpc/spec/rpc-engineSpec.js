'use strict';

import RPC from '../rpc-engine';
import {COMBINATIONS} from '../combinations';

describe('rpc-engine', () => {
    it('Should exist', () => {
        expect(RPC).toBeDefined();
        expect(typeof RPC).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should set the mode', () => {
            let rpc = new RPC('TEST');
            expect(rpc.mode).toBe('TEST');
        });
        describe('play method', () => {
            it('Should exist', () => {
                let rpc = new RPC('TEST');
                expect(rpc.play).toBeDefined();
                expect(typeof rpc.play).toBe('function');
            });
            describe('When invoked with players', () => {
                it('Should invoke the _getResults method with the players', () => {
                    let rpc = new RPC('TEST');

                    var mockPlayer = {
                        getSelection:() => 'ROCK'
                    };
                    var expectedResult = {
                        player1: {
                            selection: 'ROCK',
                            result: COMBINATIONS.ROCK.ROCK
                        },
                        player2: {
                            selection: 'ROCK',
                            result: COMBINATIONS.ROCK.ROCK
                        }
                    };
                    spyOn(rpc, '_getResults').and.callThrough();
                    spyOn(mockPlayer, 'getSelection').and.callThrough();
                    let result = rpc.play(mockPlayer, mockPlayer);
                    expect(rpc._getResults).toHaveBeenCalledWith('ROCK', 'ROCK');
                    expect(result).toEqual(expectedResult);
                });
            });
        });
    });
});