'use strict';

import {CHOICES} from '../choices';

describe('choices', () => {
    it('Should exist', () => {
        expect(CHOICES).toBeDefined();
        expect(typeof CHOICES).toBe('object');
    });
    it('Should have the correct choices', () => {
        var expected = {
            1: 'ROCK',
            2: 'PAPER',
            3: 'SCISSORS'
        };
        expect(CHOICES).toEqual(expected);
    });
});