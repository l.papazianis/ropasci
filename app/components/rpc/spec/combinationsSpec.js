'use strict';

import {COMBINATIONS} from '../combinations';
describe('combinations', () => {
    it('Should exist', () => {
        expect(COMBINATIONS).toBeDefined();
        expect(typeof COMBINATIONS).toBe('object');
    });
    it('Should have the correct combinations', () => {
        var expected = {
            'ROCK': {
                'ROCK': 'TIE',
                'PAPER': 'LOST',
                'SCISSORS': 'WON'
            },
            'PAPER': {
                'PAPER': 'TIE',
                'ROCK': 'WON',
                'SCISSORS': 'LOST'
            },
            'SCISSORS': {
                'SCISSORS': 'TIE',
                'ROCK': 'LOST',
                'PAPER': 'WON'
            }
        };
        expect(COMBINATIONS).toEqual(expected);
    });
});