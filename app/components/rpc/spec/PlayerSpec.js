'use strict';

import Player from '../Player';

describe('Player', () => {
    it('Should exist', () => {
        expect(Player).toBeDefined();
        expect(typeof Player).toBe('function');
    });
    describe('When instantiated', () => {
        it('Should have a getSelection method', () => {
            var player = new Player();
            expect(player.getSelection).toBeDefined();
            expect(typeof player.getSelection).toBe('function');
        });
        describe('When the getSelection method is invoked with a number', () => {
            it('Should return the corresponding choice', () => {
                var player = new Player();
                expect(player.getSelection(1)).toBe('ROCK');
                expect(player.getSelection(2)).toBe('PAPER');
                expect(player.getSelection(3)).toBe('SCISSORS');
            });
        });
    });
});