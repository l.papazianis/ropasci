'use strict';

import {CHOICES} from './choices';

export default class Player {
    constructor() {

    }
    getSelection(number) {
        return CHOICES[number];
    }
}