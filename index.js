/**
 * Angular 1.5.5
 * @external angular
 * @see {@link https://docs.angularjs.org/api}
 */
/**
 * @module back-office-example-component
 * @description
 * This is an example component that is generated from the cgen-back-office-template.
 * It provides a basic scaffolding for back-office components. The documentation is generated automatically using
 * annotations. Refer to JsDoc website on how to use the annotations. Each component should have its own CHANGELOG.md
 * that describes the timeline and changes of the component.
 * @example @lang js <caption>Importing the component and adding as a dependency into an angular application</caption>
 * import angular from 'angular';
 * import {componentName as exampleComponent} from 'back-office-example-component';
 * angular
 * 	.module('application', [
 * 		exampleComponent
 * 	]);
 */
export { componentName } from './app/components/example';